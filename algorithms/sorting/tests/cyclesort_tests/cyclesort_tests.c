#include "cyclesort_tests.h"

void cyclesort_tests(void)
{
        #if DEBUG
        unsigned char i;

        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        printf("Cycle Sort Tests:\n");

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        int_test(cyclesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        str_test(cyclesort);
        
        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_int_test(cyclesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_str_test(cyclesort);

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif
}
