#ifndef _STR_TEST_H_
#define _STR_TEST_H_

#include "test.h"

/*
 * Compares two strings
 *
 * Parameters:
 *      x   -   string to be compared
 *      y   -   string to be compared
 *
 * Returns:
 *      -1  -   if x < y
 *      1   -   if x > y
 *      0   -   if x == y
 */
int8_t compare_str(void const * const x, void const * const y);

/*
 *
 * Compares two string pointers
 *
 * Parameters:
 *      x   -   string pointer to be compared
 *      y   -   string pointer to be compared
 *
 * Returns:
 *      -1  -   if x < y
 *      1   -   if x > y
 *      0   -   if x == y
 */
int8_t compare_str_ptr(void const * const x, void const * const y);

/*
 * Performs a test on a sorting algorithm using an array of strings.
 *
 * Parameters:
 *      sortFunction    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 *
 */

void str_test(void (*sortFunction)(void * const, 
                                            uint64_t const, 
                                            uint64_t const, 
                                            int8_t compareFunction(void const *const, 
                                                                    void const * const)));

#endif
