#include "bubblesort_tests/bubblesort_tests.h"
#include "cyclesort_tests/cyclesort_tests.h"
#include "insertionsort_tests/insertionsort_tests.h"
#include "mergesort_tests/mergesort_tests.h"
#include "selectionsort_tests/selectionsort_tests.h"
#include "quicksort_tests/quicksort_tests.h"


int main(int argc, char **argv)
{
        for (int i = 1; i < argc; i++) {
                if (strcmp(argv[i], "bubblesort") == 0) {
                        iter_bubblesort_tests();
                        rec_bubblesort_tests();
                } else if (strcmp(argv[i], "cyclesort") == 0) {
                        cyclesort_tests();
                } else if (strcmp(argv[i], "insertionsort") == 0) {
                        iter_insertionsort_tests();
                        rec_insertionsort_tests();
                } else if (strcmp(argv[i], "mergesort") == 0) {
                        iter_ser_mergesort_tests();
                        rec_ser_mergesort_tests();
                } else if (strcmp(argv[i], "selectionsort") == 0) {
                        iter_selectionsort_tests();
                } else if (strcmp(argv[i], "quicksort") == 0) {
                        rec_ser_quicksort_first_lomuto_tests();
                        rec_ser_quicksort_middle_lomuto_tests();
                        rec_ser_quicksort_last_lomuto_tests();
                        rec_ser_quicksort_random_lomuto_tests();
                        rec_ser_quicksort_median_lomuto_tests();
                        rec_ser_quicksort_ninther_lomuto_tests();
                } else {
                        iter_bubblesort_tests();
                        rec_bubblesort_tests();
                        cyclesort_tests();
                        iter_insertionsort_tests();
                        rec_insertionsort_tests();
                        iter_ser_mergesort_tests();
                        rec_ser_mergesort_tests();
                        iter_selectionsort_tests();
                        rec_ser_quicksort_first_lomuto_tests();
                        rec_ser_quicksort_middle_lomuto_tests();
                        rec_ser_quicksort_last_lomuto_tests();
                        rec_ser_quicksort_random_lomuto_tests();
                        rec_ser_quicksort_median_lomuto_tests();
                        rec_ser_quicksort_ninther_lomuto_tests();
                }
        }
}

