#include "iter_selectionsort_test.h"
#include "selectionsort_tests.h"

void iter_selectionsort_tests(void)
{
        #if DEBUG
        unsigned char i;

        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        printf("Iterative Selection Sort Tests:\n");
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");

        clock_t t = clock();
        #endif

        int_test(iter_selectionsort);

        #if DEBUG
        t = clock() - t;
        printf("\nTime taken: %f\n", (double)t);

        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");

        t = clock();
        #endif

        str_test(iter_selectionsort);
        
        #if DEBUG
        t = clock() - t;
        printf("\nTime taken: %f\n", (double)t);

        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");

        t = clock();
        #endif

        struct_int_test(iter_selectionsort);

        #if DEBUG
        t = clock() - t;
        printf("\nTime taken: %f\n", (double)t);

        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");

        t = clock();
        #endif

        struct_str_test(iter_selectionsort);

        #if DEBUG
        t = clock() - t;
        printf("\nTime taken: %f\n", (double)t);

        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif
}
