#include "rec_insertionsort_test.h"
#include "insertionsort_tests.h"

void rec_insertionsort_tests(void)
{
        #if DEBUG
        unsigned char i;

        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        printf("Recursive Insertion Sort Tests:\n");

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        int_test(rec_insertionsort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        str_test(rec_insertionsort);
        
        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_int_test(rec_insertionsort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_str_test(rec_insertionsort);

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif
}
