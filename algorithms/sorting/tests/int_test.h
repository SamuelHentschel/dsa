#ifndef _INT_TEST_H_
#define _INT_TEST_H_

#include "test.h"

/*
 * Compares two integers.
 *
 * Parameters:
 *      x   -   integer to be compared
 *      y   -   integer to be compared
 *
 * Returns:
 *      -1  -   if x < y
 *      1   -   if x > y
 *      0   -   if x == y
 */
int8_t compare_int(void const * const x, void const * const y);

/*
 * Performs a test on a sorting algorithm using an array of integers.
 *
 * Parameters:
 *      sort_function    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 */
void int_test(void (*sort_function)(void * const, 
                                            uint64_t const, 
                                            uint64_t const, 
                                            int8_t compare_function(void const *const, 
                                                                    void const * const)));
#endif
