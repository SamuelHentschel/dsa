#include "iter_ser_mergesort_test.h"
#include "mergesort_tests.h"

void iter_ser_mergesort_tests(void)
{
        #if DEBUG
        unsigned char i;

        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        printf("Iterative Serial Merge Sort Tests:\n");

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        int_test(iter_ser_mergesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        str_test(iter_ser_mergesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_int_test(iter_ser_mergesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_str_test(iter_ser_mergesort);

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif
}
