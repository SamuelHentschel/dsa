#include "iter_bubblesort_test.h"
#include "bubblesort_tests.h"

void iter_bubblesort_tests(void)
{
        #if DEBUG
        unsigned char i;
        
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        printf("Iterative Bubble Sort Tests:\n");

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        int_test(iter_bubblesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        str_test(iter_bubblesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_int_test(iter_bubblesort);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_str_test(iter_bubblesort);

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif
}
