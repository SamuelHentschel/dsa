#include "struct_test.h"

typedef struct {
        int x;
        char *str;
} test;

/*
 * Compares two structs using the integer within.
 *
 * Parameters:
 *      x   -   struct to be compared
 *      y   -   struct to be compared
 *
 * Returns:
 *      -1  -   if x.x < y.x
 *      1   -   if x.x > y.x
 *      0   -   if x.x == y.x
 */
int8_t compare_struct_int(void const * const x, void const * const y)
{
        if (((test *)x)->x < ((test *)y)->x)
                return -1;
        else if (((test *)x)->x > ((test *)y)->x)
                return 1;
        else
                return 0;
}

/*
 * Compares two structs using the string within.
 *
 * Parameters:
 *      x   -   struct to be compared
 *      y   -   struct to be compared
 *
 * Returns:
 *      -1  -   if x.str < y.str
 *      1   -   if x.str > y.str
 *      0   -   if x.str == y.str
 */
int8_t compare_struct_str(void const * const x, 
                                        void const * const y)
{
        return strcmp(((test *)x)->str, ((test *)y)->str);
}

/*
 * Performs a test on a sorting algorithm using an array of test structs using 
 * the integer within.
 *
 * Parameters:
 *      sortFunction    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 */
void struct_int_test(void (*sortFunction)(void * const, 
                                                    uint64_t const, 
                                                    uint64_t const, 
                                                    int8_t compareFunction(void const *const, 
                                                                            void const * const)))
{
// VARIABLES ===================================================================
        test struct_arr_int[] = { {.x = 3, .str = "sample"}, {.x = 12, .str = "apples"}, 
                            {.x = 4, .str = "cranks"}, {.x = 32, .str = "tanker"},
                            {.x = 20, .str = "flipit"}, {.x = 1, .str = "cantor"}, 
                            {.x = 5, .str = "strips"}, {.x = 9, .str = "clamps"},
                            {.x = 2, .str = "flower"}, {.x = 10, .str = "dancer"} };
        test struct_arr_int_sol[] = { {.x = 1, .str = "cantor"},  {.x = 2, .str = "flower"},
                            {.x = 3, .str = "sample"}, {.x = 4, .str = "cranks"},
                            {.x = 5, .str = "strips"}, {.x = 9, .str = "clamps"},
                            {.x = 10, .str = "dancer"},  {.x = 12, .str = "apples"},
                            {.x = 20, .str = "flipit"}, {.x = 32, .str = "tanker"} }; 

        test *struct_arr_int_ptr = calloc(sizeof(test), 10);
        for (int i = 0; i < 10; i++) {
                struct_arr_int_ptr[i].x = struct_arr_int[i].x;
                struct_arr_int_ptr[i].str = struct_arr_int[i].str;
        }

// TESTS =======================================================================
        #if DEBUG
        printf("test structures (int):\n");
        printf("\tstructure array before sort:");
        for (uint8_t i = 0; i < 10; i++)
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_int[i].x, struct_arr_int[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_int[i].x, struct_arr_int[i].str);
        printf("\n\tdynamic structure array before sort:");
        for (uint8_t i = 0; i < 10; i++)
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_int_ptr[i].x, struct_arr_int_ptr[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_int_ptr[i].x, struct_arr_int_ptr[i].str);
        printf("\n\n");
        #endif

        sortFunction(struct_arr_int, 10, sizeof(test), compare_struct_int);
        sortFunction(struct_arr_int_ptr, 10, sizeof(test), compare_struct_int);

        #if DEBUG
        printf("\tstructure array after sort:");
        for (uint8_t i = 0; i < 10; i++)
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_int[i].x, struct_arr_int[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_int[i].x, struct_arr_int[i].str);
        printf("\n\tdynamic structure array after sort:");
        for (uint8_t i = 0; i < 10; i++)
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_int_ptr[i].x, struct_arr_int_ptr[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_int_ptr[i].x, struct_arr_int_ptr[i].str);
        printf("\n");
        #endif
        for (uint8_t i = 0; i < 10; i++) {
                assert(struct_arr_int[i].x == struct_arr_int_sol[i].x);
                assert(!strcmp(struct_arr_int[i].str, struct_arr_int_sol[i].str));
                assert(struct_arr_int_ptr[i].x == struct_arr_int_sol[i].x);
                assert(!strcmp(struct_arr_int_ptr[i].str, struct_arr_int_sol[i].str));
        }

// FREE ========================================================================
        free(struct_arr_int_ptr);
}

/*
 * Performs a test on a sorting algorithm using an array of test structs using 
 * the string within.
 *
 * Parameters:
 *      sortFunction    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 */
void struct_str_test(void (*sortFunction)(void * const, 
                                                    uint64_t const, 
                                                    uint64_t const, 
                                                    int8_t compareFunction(void const *const, 
                                                                            void const * const)))
{
// VARIABLES ===================================================================
        test struct_arr_str[] = { {.x = 3, .str = "sample"}, {.x = 12, .str = "apples"}, 
                            {.x = 4, .str = "cranks"}, {.x = 32, .str = "tanker"},
                            {.x = 20, .str = "flipit"}, {.x = 1, .str = "cantor"}, 
                            {.x = 5, .str = "strips"}, {.x = 9, .str = "clamps"},
                            {.x = 2, .str = "flower"}, {.x = 10, .str = "dancer"} };
        test struct_arr_str_sol[] = { {.x = 12, .str = "apples"}, {.x = 1, .str = "cantor"},
                            {.x = 9, .str = "clamps"}, {.x = 4, .str = "cranks"},
                            {.x = 10, .str = "dancer"}, {.x = 20, .str = "flipit"},
                            {.x = 2, .str = "flower"}, {.x = 3, .str = "sample"},
                            {.x = 5, .str = "strips"}, {.x = 32, .str = "tanker"} };

        test *struct_arr_str_ptr = calloc(sizeof(test), 10);
        for (int i = 0; i < 10; i++) {
                struct_arr_str_ptr[i].x = struct_arr_str[i].x;
                struct_arr_str_ptr[i].str = struct_arr_str[i].str;
        }

// TESTS =======================================================================
        #if DEBUG
        printf("test structures (str):\n");
        printf("\tstructure array before sort:");
        for (uint8_t i = 0; i < 10; i++) 
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_str[i].x, struct_arr_str[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_str[i].x, struct_arr_str[i].str);
        printf("\n\tdynamic structure array before sort:");
        for (uint8_t i = 0; i < 10; i++) 
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_str_ptr[i].x, struct_arr_str_ptr[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_str_ptr[i].x, struct_arr_str_ptr[i].str);
        printf("\n\n");
        #endif

        sortFunction(struct_arr_str, 10, sizeof(test), compare_struct_str);
        sortFunction(struct_arr_str_ptr, 10, sizeof(test), compare_struct_str);

        #if DEBUG
        printf("\tstructure array after sort:");
        for (uint8_t i = 0; i < 10; i++)
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_str[i].x, struct_arr_str[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_str[i].x, struct_arr_str[i].str);
        printf("\n\tdynamic structure array after sort:");
        for (uint8_t i = 0; i < 10; i++)
                if (i % 5 == 0)
                        printf("\n\t\t{ %d, %s } ", struct_arr_str_ptr[i].x, struct_arr_str_ptr[i].str);
                else
                        printf("{ %d, %s } ", struct_arr_str_ptr[i].x, struct_arr_str_ptr[i].str);
        printf("\n");
        #endif
        for (uint8_t i = 0; i < 10; i++) {
                assert(struct_arr_str[i].x == struct_arr_str_sol[i].x);
                assert(!strcmp(struct_arr_str[i].str, struct_arr_str_sol[i].str));
                assert(struct_arr_str_ptr[i].x == struct_arr_str_sol[i].x);
                assert(!strcmp(struct_arr_str_ptr[i].str, struct_arr_str_sol[i].str));
        }

// FREE ========================================================================
        free(struct_arr_str_ptr);
}
