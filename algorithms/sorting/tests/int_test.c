#include "int_test.h"

/*
 * Compares two integers.
 *
 * Parameters:
 *      x   -   integer to be compared
 *      y   -   integer to be compared
 *
 * Returns:
 *      -1  -   if x < y
 *      1   -   if x > y
 *      0   -   if x == y
 */
int8_t compare_int(void const * const x, void const * const y)
{
        if (*(int *)x < *(int *)y)
                return -1;
        else if (*(int *)x > *(int *)y)
                return 1;
        else
                return 0;
}



static void int_array_ptr_test(void (*sort_function)(void * const, 
                                uint64_t const,
                                uint64_t const,
                                int8_t (*compare_function)(void const * const,
                                        void const * const)),
                        int num)
{
// VARIABLES ===================================================================
        int *int_arr = calloc(sizeof(int), num);
        for (int i = 0; i < num; i++)
                int_arr[i] = rand() % num;

// TESTS [10] ==================================================================
        printf("\ttest dynamic integer array[%d]:\n", num);
        #if DEBUG
        printf("\tdynamic integer array[%d] before sort:\n\t\t", num);
        for (int i = 0; i < num; i++)
                printf("%d ", int_arr[i]);
        #endif

        clock_t t = clock();
        sort_function(int_arr, num, sizeof(int), compare_int);
        t = clock() - t;

        #if DEBUG
        printf("\tdynamic integer array[%d] after sort:\n\t\t", num);
        for (int i = 0; i < num; i++)
                printf("%d ", int_arr[i]);
        #endif

        for (int i = 0; i < num - 1; i++)
                assert(int_arr[i] <= int_arr[i + 1]);

        printf("\t...passed. (%f seconds)\n", (double)t / CLOCKS_PER_SEC);

// FREE ========================================================================
        free(int_arr);
}

static void int_array_test(void (*sort_function)(void * const, 
                                uint64_t const,
                                uint64_t const,
                                int8_t (*compare_function)(void const * const,
                                        void const * const)),
                        int num)
{
// VARIABLES ===================================================================
        int int_arr[num];
        for (int i = 0; i < num; i++)
                int_arr[i] = rand() % num;

// TESTS =======================================================================
        printf("\ttest static integer array[%d]:\n", num);
        #if DEBUG
        printf("\tstatic integer array[%d] before sort:\n\t\t", num);
        for (int i = 0; i < num; i++)
                printf("%d ", int_arr[i]);
        #endif

        clock_t t = clock();
        sort_function(int_arr, num, sizeof(int), compare_int);
        t = clock() - t;

        #if DEBUG
        printf("\tstatic integer array[%d] after sort:\n\t\t", num);
        for (int i = 0; i < num; i++)
                printf("%d ", int_arr[i]);
        #endif

        for (int i = 0; i < num; i++)
                assert(int_arr[i] <= int_arr[i + 1]);

        printf("\t...passed. (%f seconds)\n", (double)t / CLOCKS_PER_SEC);
}

/*
 * Calls functions to perform tests on sorting functions using integer arrays
 * (both static and dynamic).
 *
 * Parameters:
 *      sort_function    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 */
void int_test(void (*sort_function)(void * const, 
                                            uint64_t const, 
                                            uint64_t const, 
                                            int8_t (*compare_function)(void const *const, 
                                                                    void const * const)))
{
        for (int i = 0; i < 100; i += 10) {
                int_array_test(sort_function, i);
                int_array_ptr_test(sort_function, i);
        }

        for (int i = 100; i <= 1000; i += 100) {
                int_array_test(sort_function, i);
                int_array_ptr_test(sort_function, i);
        }
}
