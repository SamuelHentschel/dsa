#ifndef _STRUCT_TEST_H_
#define _STRUCT_TEST_H_

#include "test.h"

/*
 * Compares two structs using the integer within.
 *
 * Parameters:
 *      x   -   struct to be compared
 *      y   -   struct to be compared
 *
 * Returns:
 *      -1  -   if x.x < y.x
 *      1   -   if x.x > y.x
 *      0   -   if x.x == y.x
 */
int8_t compare_struct_int(void const * const x, void const * const y);

/*
 * Compares two structs using the string within.
 *
 * Parameters:
 *      x   -   struct to be compared
 *      y   -   struct to be compared
 *
 * Returns:
 *      -1  -   if x.str < y.str
 *      1   -   if x.str > y.str
 *      0   -   if x.str == y.str
 */
int8_t compare_struct_str(void const * const x, 
                                        void const * const y);

/*
 * Performs a test on a sorting algorithm using an array of test structs using 
 * the integer within.
 *
 * Parameters:
 *      sortFunction    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 */
void struct_int_test(void (*sortFunction)(void * const, 
                                                    uint64_t const, 
                                                    uint64_t const, 
                                                    int8_t compareFunction(void const *const, 
                                                                            void const * const)));

/*
 * Performs a test on a sorting algorithm using an array of test structs using 
 * the string within.
 *
 * Parameters:
 *      sortFunction    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 */
void struct_str_test(void (*sortFunction)(void * const, 
                                                    uint64_t const, 
                                                    uint64_t const, 
                                                    int8_t compareFunction(void const *const, 
                                                                            void const * const)));

#endif
