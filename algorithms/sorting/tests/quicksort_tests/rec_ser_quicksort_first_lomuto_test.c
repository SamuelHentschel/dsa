#include "quicksort_tests.h"

void rec_ser_quicksort_first_lomuto_tests(void)
{
        #if DEBUG
        unsigned char i;

        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        printf("Recursive Serial Quicksort (Pivot: First - Partition: Lomuto Tests:\n");

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif

        int_test(rec_ser_quicksort_first_lomuto);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        str_test(rec_ser_quicksort_first_lomuto);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_int_test(rec_ser_quicksort_first_lomuto);

        #if DEBUG
        for (i = 0; i < 40; i++)
                printf("-");
        printf("\n");
        #endif

        struct_str_test(rec_ser_quicksort_first_lomuto);

        #if DEBUG
        for (i = 0; i < 81; i++)
                printf("=");
        printf("\n");
        #endif
}
