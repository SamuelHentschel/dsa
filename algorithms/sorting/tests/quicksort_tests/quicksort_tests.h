#ifndef _QUICKSORT_TESTS_H_
#define _QUICKSORT_TESTS_H_

#include "../test.h"

#include "rec_ser_quicksort_first_lomuto_test.h"
#include "rec_ser_quicksort_middle_lomuto_test.h"
#include "rec_ser_quicksort_last_lomuto_test.h"
#include "rec_ser_quicksort_random_lomuto_test.h"
#include "rec_ser_quicksort_median_lomuto_test.h"
#include "rec_ser_quicksort_ninther_lomuto_test.h"

#include "../../include/quicksort.h"

#endif
