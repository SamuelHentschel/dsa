#include "str_test.h"


/*
 * Compares two strings
 *
 * Parameters:
 *      x   -   string to be compared
 *      y   -   string to be compared
 *
 * Returns:
 *      -1  -   if x < y
 *      1   -   if x > y
 *      0   -   if x == y
 */
int8_t compare_str(void const * const x, void const * const y)
{
        return strcmp((char *)x, (char *)y);
}

/*
 *
 * Compares two string pointers
 *
 * Parameters:
 *      x   -   string pointer to be compared
 *      y   -   string pointer to be compared
 *
 * Returns:
 *      -1  -   if x < y
 *      1   -   if x > y
 *      0   -   if x == y
 */
int8_t compare_str_ptr(void const * const x, void const * const y)
{
        return strcmp(*(char **)x, *(char **)y);
}

/*
 * Performs a test on a sorting algorithm using an array of strings.
 *
 * Parameters:
 *      sortFunction    -   the sorting function to test it takes a base pointer,
 *                          number of elements, size of elements, and a comparison
 *                          function that takes two pointers to elements to 
 *                          compare.
 *
 */

void str_test(void (*sortFunction)(void * const, 
                                            uint64_t const, 
                                            uint64_t const, 
                                            int8_t compareFunction(void const *const, 
                                                                    void const * const)))
{
// VARIABLES ===================================================================
        char str_arr[][7] = {"sample", "apples", "cranks", "tanker", "flipit", "cantor",
                            "strips", "clamps", "flower", "dancer"};
        char str_arr_sol[][7] = {"apples", "cantor", "clamps", "cranks", "dancer",
                            "flipit", "flower", "sample", "strips", "tanker"};

        char **str_arr_ptr = calloc(sizeof(char *), 10);
        for (uint8_t i = 0; i < 10; i++) {
                char *temp = calloc(sizeof(char), strlen(str_arr[i]));
                strcpy(temp, str_arr[i]);
                str_arr_ptr[i] = temp;
        }

// TESTS =======================================================================
        #if DEBUG
        printf("test string array:\n");
        printf("\tstring array before sort:\n\t\t");
        for (uint8_t i = 0; i < 10; i++)
                printf("%s ", str_arr[i]);
        printf("\n\tdynamic string array before sort:\n\t\t");
        for (uint8_t i = 0; i < 10; i++)
                printf("%s ", str_arr[i]);
        printf("\n\n");
        #endif

        sortFunction(str_arr, 10, 7, compare_str);
        sortFunction(str_arr_ptr, 10, sizeof(char *), compare_str_ptr);

        #if DEBUG
        printf("\tstring array after sort:\n\t\t");
        for (uint8_t i = 0; i < 10; i++)
                printf("%s ", str_arr[i]);
        printf("\n\tdynamic string array after sort:\n\t\t");
        for (uint8_t i = 0; i < 10; i++)
                printf("%s ", str_arr_ptr[i]);
        printf("\n");
        #endif

        for (uint8_t i = 0; i < 10; i++) {
                assert(!strcmp(str_arr[i], str_arr_sol[i]));
                assert(!strcmp(str_arr_ptr[i], str_arr_sol[i]));
        }

// FREE ========================================================================
        for (uint8_t i = 0; i < 10; i++)
                free(str_arr_ptr[i]);
        free(str_arr_ptr);
}
