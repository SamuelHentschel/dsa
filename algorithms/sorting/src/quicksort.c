#include "../include/sort_utils.h"
#include "../include/quicksort.h"

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

static uint64_t first(__attribute__((unused)) void * const base,
                __attribute__((unused)) uint64_t const nitems,
                __attribute__((unused)) uint64_t const size,
                __attribute__((unused)) int8_t (*compare)(void const * const, void const * const))
{
        return 0;
}

static uint64_t middle(__attribute__((unused)) void * const base,
                uint64_t const nitems,
                __attribute__((unused)) uint64_t const size,
                __attribute__((unused)) int8_t (*compare)(void const * const, void const * const))
{
        return nitems / 2;
}

static uint64_t last(__attribute__((unused)) void * const base,
                uint64_t const nitems,
                __attribute__((unused)) uint64_t const size,
                __attribute__((unused)) int8_t (*compare)(void const * const, void const * const))
{
        return nitems - 1;
}

static uint64_t random(__attribute__((unused)) void * const base,
                uint64_t const nitems,
                __attribute__((unused)) uint64_t const size,
                __attribute__((unused)) int8_t (*compare)(void const * const, void const * const))
{
        uint64_t r = rand();
        return ((r << 32) | rand()) % nitems;
}

static uint64_t median(void * const base,
                uint64_t const nitems,
                uint64_t const size,
                int8_t (*compare)(void const * const, void const * const))
{
        uint64_t mid = nitems / 2;

        if (compare(((uint8_t *)base) + ((nitems - 1) * size), base) < 0)
                swap(((uint8_t *)base) + ((nitems - 1) * size), base, size);

        if (compare(((uint8_t *)base) + ((nitems / 2) * size), base) < 0)
                swap(((uint8_t *)base) + ((nitems / 2) * size), base, size);

        if (compare(((uint8_t *)base) + ((nitems - 1) * size), ((uint8_t *)base) + ((nitems / 2) * size)) < 0)
                swap(((uint8_t *)base) + ((nitems - 1) * size), ((uint8_t *)base) + ((nitems / 2) * size), size);

        return mid;
}

static uint64_t ninther(void * const base,
                uint64_t const nitems,
                uint64_t const size,
                int8_t (*compare)(void const * const, void const * const))
{
        uint64_t med_left = median(base, nitems / 3, size, compare);
        uint64_t med_mid = median(((uint8_t *)base) + ((nitems / 3)* size), nitems / 3, size, compare);
        uint64_t med_right = median(((uint8_t *)base) + (2 * (nitems / 3) * size), nitems - (2 * (nitems / 3)), size, compare);

        if (compare(((uint8_t *)base) + (((2 * (nitems / 3)) + med_right) * size), ((uint8_t *)base) + (med_left * size)) < 0)
                swap(((uint8_t *)base) + (((2 * (nitems / 3)) + med_right) * size), ((uint8_t *)base) + (med_left * size), size);

        if (compare(((uint8_t *)base) + (((nitems / 3) + med_mid) * size), ((uint8_t *)base) + (med_left * size)) < 0)
                swap(((uint8_t *)base) + (((nitems / 3) + med_mid) * size), ((uint8_t *)base) + (med_left * size), size);

        if (compare(((uint8_t *)base) + (((2 * (nitems / 3)) + med_right) * size), ((uint8_t *)base) + (((nitems / 3) + med_mid) * size)) < 0)
                swap(((uint8_t *)base) + (((2 * (nitems / 3)) + med_right) * size), ((uint8_t *)base) + (((nitems / 3) + med_mid) * size), size);

        return (nitems / 3) + med_mid;
}

static uint64_t lomuto(void * const base,
                uint64_t const nitems,
                uint64_t const size,
                int8_t (*compare)(void const * const, void const * const),
                uint64_t (*pivot_func)(void * const base,
                uint64_t const nitems,
                uint64_t const size,
                int8_t (*compare)(void const * const, void const * const)))
{
        uint64_t pivot = pivot_func(base, nitems, size, compare);

        swap(((uint8_t *)base) + ((nitems - 1) * size), ((uint8_t *)base) + (pivot * size), size);
        pivot = nitems - 1;

        uint64_t i = 0;

        for (uint64_t j = 0; j < nitems - 1; j++) {
                if (compare(((uint8_t *)base) + (j * size), ((uint8_t *)base) + (pivot * size)) < 0)
                        swap(((uint8_t *)base) + (i++ * size), ((uint8_t *)base) + (j * size), size);
        }

        swap(((uint8_t *)base) + (i * size), ((uint8_t *)base) + ((nitems - 1) * size), size);

        return i;
}

static void rec_ser_quicksort(void * const base, 
                        uint64_t const nitems, 
                        uint64_t const size, 
                        int8_t (*compare)(void const * const, void const * const),


                        uint64_t (*partition)(void * const base,
                        uint64_t const nitems,
                        uint64_t const size,
                        int8_t (*compare)(void const * const, void const * const),

                        uint64_t (*pivot_func)(void * const base,
                        uint64_t const nitems,
                        uint64_t const size,
                        int8_t (*compare)(void const * const, void const * const))),


                        uint64_t (*pivot_func)(void * const base,
                        uint64_t const nitems,
                        uint64_t const size,
                        int8_t (*compare)(void const * const, void const * const)))
{
        if (nitems < 2)
                return;

        uint64_t part = partition(base, nitems, size, compare, pivot_func);

        rec_ser_quicksort(base, part, size, compare, partition, pivot_func);
        rec_ser_quicksort(((uint8_t *)base) + ((part + 1) * size), nitems - (part + 1), size, compare, partition, pivot_func);
}

///*
// * Performs iterative serial quicksort on an array using the first element as the
// * pivot and lomuto's partitioning.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_first_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the middle element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_middle_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the last element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_last_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using a random element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_random_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the median element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_median_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the ninther as the pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_ninther_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}

/*
 * Performs recursive serial quicksort on an array using the first element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_first_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        rec_ser_quicksort(base, nitems, size, compare, lomuto, first);
}

/*
 * Performs recursive serial quicksort on an array using the middle element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_middle_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        rec_ser_quicksort(base, nitems, size, compare, lomuto, middle);
}

/*
 * Performs recursive serial quicksort on an array using the last element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_last_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        rec_ser_quicksort(base, nitems, size, compare, lomuto, last);
}

/*
 * Performs recursive serial quicksort on an array using a random element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_random_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        rec_ser_quicksort(base, nitems, size, compare, lomuto, random);
}

/*
 * Performs recursive serial quicksort on an array using the median element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_median_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        rec_ser_quicksort(base, nitems, size, compare, lomuto, median);
}

/*
 * Performs recursive serial quicksort on an array using the ninther as the pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_ninther_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        rec_ser_quicksort(base, nitems, size, compare, lomuto, ninther);
}

///*
// * Performs iterative serial quicksort on an array using the first element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_first_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the middle element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_middle_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the last element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_last_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using a random element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_random_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the median element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_median_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the ninther as the pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_ninther_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the first element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_first_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the middle element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_middle_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the last element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_last_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using a random element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_random_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the median element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_median_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the ninther as the pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_ninther_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
