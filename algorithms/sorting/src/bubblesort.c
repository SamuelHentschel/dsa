#include "../include/sort_utils.h"
#include "../include/bubblesort.h"

/*
 * Performs iterative bubblesort on an array of items.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void iter_bubblesort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        for (uint64_t i = 0; i < nitems - 1; i++) {
                for (uint64_t j = 0; j < nitems - i - 1; j++) {
                        if (compare(((uint8_t *)base) + (j * size), ((uint8_t *)base) + ((j + 1) * size)) > 0)
                                swap(((uint8_t *)base) + (j * size), ((uint8_t *)base) + ((j + 1) * size), size);
                }
        }
}

/*
 * Performs recursive bubblesort on an array of items.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_bubblesort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        if (nitems == 1)
                return;

        for (uint64_t i = 0; i < nitems - 1; i++) {
                if (compare(((uint8_t *)base) + (i * size), ((uint8_t *)base) + ((i + 1) * size)) > 0)
                        swap(((uint8_t *)base) + (i * size), ((uint8_t *)base) + ((i + 1) * size), size);
        }

        rec_bubblesort(base, nitems - 1, size, compare);
}
