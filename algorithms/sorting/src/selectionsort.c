#include "../include/sort_utils.h"
#include "../include/selectionsort.h"

/*
 * Performs selectionsort on an array of items.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void iter_selectionsort(void * const base, 
                    uint64_t const nitems, 
                    uint64_t const size, 
                    int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        for (uint64_t i = 0; i < nitems; i++) {
                void *smallest = ((uint8_t *)base) + (i * size);

                for (uint64_t j = i + 1; j < nitems; j++) {
                        if (compare(smallest, ((uint8_t *)base) + (j * size)) > 0)
                                smallest = ((uint8_t *)base) + (j * size);
                }

                swap(((uint8_t *)base) + (i * size), smallest, size);
        }
}

