#include "../include/sort_utils.h"

/*
 * Swaps two objects in memory.
 *
 * Parameters:
 *      x       -   object to be swapped
 *      y       -   object to be swapped
 *      size    -   size of the object to be swapped
 */
void swap(void * const x, void * const y, uint64_t const size)
{
        if (!(x && y && size))
                return;

        uint8_t *_x = (uint8_t *)x;
        uint8_t *_y = (uint8_t *)y;

        for (uint64_t i = 0; i < size; i++) {
                uint8_t temp = *_x;
                *_x++ = *_y;
                *_y++ = temp;
        }
}

/*
 * Set one object's value to another object's value.
 *
 * Parameters:
 *      x       -   object who's value is to be set
 *      y       -   object who's value is put in x
 *      size    -   size of the object to be set
 */
void set(void * const x, void * const y, uint64_t const size)
{
        if (!(x && y && size))
                return;

        uint8_t *_x = (uint8_t *)x;
        uint8_t *_y = (uint8_t *)y;

        for (uint64_t i = 0; i < size; i++)
                *_x++ = *_y++;
}
