#include "../include/sort_utils.h"
#include "../include/cyclesort.h"

#include <stdlib.h>

/*
 * Performs cyclesort on an array.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void cyclesort(void * const base,
                uint64_t const nitems,
                uint64_t const size,
                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        for (uint64_t cycle_start = 0; cycle_start < nitems - 1; cycle_start++) {
                void *curr_elem = malloc(size);
                uint64_t pos = cycle_start;

                set(curr_elem, ((uint8_t *)base) + (cycle_start * size), size);

                for (uint64_t i = cycle_start + 1; i < nitems; i++) {
                        if (compare(((uint8_t *)base) + (i * size), curr_elem) < 0)
                                pos++;
                }

                if (pos == cycle_start)
                        continue;

                for (; compare(curr_elem, ((uint8_t *)base) + (pos * size)) == 0; pos++);
                swap(((uint8_t *)base) + (pos * size), curr_elem, size);

                for (; pos != cycle_start;) {
                        pos = cycle_start;

                        for (uint64_t i = cycle_start + 1; i < nitems; i++) {
                                if (compare(((uint8_t *)base) + (i * size), curr_elem) < 0)
                                        pos++;
                        }

                        for (; compare(curr_elem, ((uint8_t *)base) + (pos * size)) == 0; pos++);
                        swap(((uint8_t *)base) + (pos * size), curr_elem, size);
                }

                free(curr_elem);
        }
}
