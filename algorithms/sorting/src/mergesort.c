#include "../include/sort_utils.h"
#include "../include/mergesort.h"

#include <stdlib.h>

/*
 * Merges two sets of arrays together.
 * Parameters:
 *      base            -  a pointer to the left array to be merged
 *      middle          -  the index of the first element in the right array
 *                         with respect to the left array's first element
 *      nitems          -  the number of items 
 *      size            -  the total number of elements in both arrays
 *      compare         -  a comparison function given by the user
 */
static void merge(void * const base,
                uint64_t const middle,
                uint64_t const nitems,
                uint64_t const size,
                int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        void *L = malloc(middle * size);
        void *R = malloc((nitems - middle) * size);
        uint64_t i;
        uint64_t j;
        uint64_t k;

        for (i = 0; i < middle; i++)
                set(((uint8_t *)L) + (i * size), ((uint8_t *)base) + (i * size), size);
        for (i = 0; i < nitems - middle; i++)
                set(((uint8_t *)R) + (i * size), ((uint8_t *)base) + ((middle + i) * size), size);

        for (i = 0, j = 0, k = 0; i < middle && j < nitems - middle; k++) {
                if (compare(((uint8_t *)L) + (i * size), ((uint8_t *)R) + (j * size)) <= 0)
                        set(((uint8_t *)base) + (k * size), ((uint8_t *)L) + (i++ * size), size);
                else
                        set(((uint8_t *)base) + (k * size), ((uint8_t *)R) + (j++ * size), size);
        }

        
        for (; i < middle; i++, k++)
                set(((uint8_t *)base) + (k * size), ((uint8_t *)L) + (i * size), size);

        for (; j < nitems - middle; j++, k++)
                set(((uint8_t *)base) + (k * size), ((uint8_t *)R) + (j * size), size);

        free(L);
        free(R);
}

/*
 * Performs iterative serial mergesort on an array.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void iter_ser_mergesort(void * const base,
                        uint64_t const nitems,
                        uint64_t const size,
                        int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        uint64_t curr_size;
        uint64_t left;

        for (curr_size = 1; curr_size < nitems; curr_size *= 2) {
                for (left = 0; left < nitems - 1; left += 2 * curr_size) {
                        uint64_t middle = curr_size < nitems - left ? curr_size : nitems - left;

                        merge(((uint8_t *)base) + (left * size), middle, (curr_size * 2) < nitems - left ? curr_size * 2 : nitems- left, size, compare);
                }
        }
}

/*
 * Performs recursive serial mergesort on an array.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_mergesort(void * const base,
                        uint64_t const nitems,
                        uint64_t const size,
                        int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        if (nitems < 2)
                return;

        uint64_t middle = nitems / 2;

        rec_ser_mergesort(base, middle, size, compare);
        rec_ser_mergesort(((uint8_t *)base) + (middle * size), nitems - middle, size, compare);
        merge(base, middle, nitems, size, compare);
}

