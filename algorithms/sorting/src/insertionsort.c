#include "../include/sort_utils.h"
#include "../include/insertionsort.h"

#include <stdlib.h>

/*
 * Performs iterative insertionsort on an array of items.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void iter_insertionsort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        void *key = malloc(size);

        for (uint64_t i = 1; i < nitems; i++) {
                set(key, ((uint8_t *)base) + (i * size), size);

                int64_t j = i - 1;

                for (; j >= 0 && compare(key, ((uint8_t *)base) + (j * size)) < 0; j--)
                        set(((uint8_t *)base) + ((j + 1) * size), ((uint8_t *)base) + (j * size), size);

                set(((uint8_t *)base) + ((j + 1) * size), key, size);
        }

        free(key);
}

/*
 * Performs recursive insertionsort on an array of items; recursively.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_insertionsort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const))
{
        if (!(base && nitems && size && compare))
                return;

        if (nitems == 1)
                return;

        rec_insertionsort(base, nitems - 1, size, compare);

        void *last = malloc(size);

        set(last, ((uint8_t *)base) + ((nitems - 1) * size), size);

        int64_t i = nitems - 2;

        for (; i >= 0 && compare(((uint8_t *)base) + (i * size), last) > 0; i--)
                set(((uint8_t *)base) + ((i + 1) * size), ((uint8_t *)base) + (i * size), size);

        set(((uint8_t *)base) + ((i + 1) * size), last, size);

        free(last);
}
