#ifndef _MERGESORT_H_
#define _MERGESORT_H_
#include <stdint.h>

/*
 * Performs iterative serial mergesort on an array of items.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void iter_ser_mergesort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const));

/*
 * Performs recursive serial mergesort on an array of items; recursively.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_mergesort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const));
#endif
