#ifndef _QUICKSORT_H_
#define _QUICKSORT_H_

///*
// * Performs iterative serial quicksort on an array using the first element as the
// * pivot and lomuto's partitioning.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_first_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the middle element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_middle_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the last element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_last_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using a random element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_random_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the median element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_median_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the ninther as the pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_ninther_lomuto(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}

/*
 * Performs recursive serial quicksort on an array using the first element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_first_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const));

/*
 * Performs recursive serial quicksort on an array using the middle element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_middle_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const));

/*
 * Performs recursive serial quicksort on an array using the last element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_last_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const));

/*
 * Performs recursive serial quicksort on an array using a random element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_random_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const));

/*
 * Performs recursive serial quicksort on an array using the median element as the
 * pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_median_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const));

/*
 * Performs recursive serial quicksort on an array using the ninther as the pivot.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_ser_quicksort_ninther_lomuto(void * const base,
                                uint64_t const nitems,
                                uint64_t const size,
                                int8_t (*compare)(void const * const, void const * const));

///*
// * Performs iterative serial quicksort on an array using the first element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_first_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the middle element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_middle_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the last element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_last_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using a random element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_random_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the median element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_median_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs iterative serial quicksort on an array using the ninther as the pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void iter_ser_quicksort_ninther_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the first element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_first_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the middle element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_middle_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the last element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_last_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using a random element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_random_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the median element as the
// * pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_median_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
//
///*
// * Performs recursive serial quicksort on an array using the ninther as the pivot.
// * Parameters:
// *      base        -   a pointer to the array of items to be sorted
// *      nitems      -   number of items in the array
// *      size        -   size of an item in the array
// *      compare     -   function to compare the two array elements
// */
//void rec_ser_quicksort_ninther_hoare(void * const base,
//                                uint64_t const nitems,
//                                uint64_t const size,
//                                int8_t (*compare)(void const * const, void const * const))
//{
//}
#endif
