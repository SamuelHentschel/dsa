#ifndef _INSERTIONSORT_H_
#define _INSERTIONSORT_H_
#include <stdint.h>

/*
 * Performs iterative insertionsort on an array of items.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void iter_insertionsort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const));

/*
 * Performs recursive insertionsort on an array of items; recursively.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void rec_insertionsort(void * const base,
                    uint64_t const nitems,
                    uint64_t const size,
                    int8_t (*compare)(void const * const, void const * const));
#endif
