#ifndef _CYCLESORT_H_
#define _CYCLESORT_H_
/*
 * Performs cyclesort on an array.
 * Parameters:
 *      base        -   a pointer to the array of items to be sorted
 *      nitems      -   number of items in the array
 *      size        -   size of an item in the array
 *      compare     -   function to compare the two array elements
 */
void cyclesort(void * const base,
                uint64_t const nitems,
                uint64_t const size,
                int8_t (*compare)(void const * const, void const * const));
#endif
