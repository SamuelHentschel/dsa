#ifndef _SORT_UTILS_H_
#define _SORT_UTILS_H_
#include <stdint.h>

/*
 * Swaps two objects in memory.
 *
 * Parameters:
 *      x       -   object to be swapped
 *      y       -   object to be swapped
 *      size    -   size of the object to be swapped
 */
void swap(void * const x, void * const y, uint64_t const size);

/*
 * Set one object's value to another object's value.
 *
 * Parameters:
 *      x       -   object who's value is to be set
 *      y       -   object who's value is put in x
 *      size    -   size of the object to be set
 */
void set(void * const x, void * const y, uint64_t const size);
#endif
