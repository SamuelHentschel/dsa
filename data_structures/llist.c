#include "llist.h"

#include <stdlib.h>

llist *llist_init() {
    llist *list = (llist *)calloc(1, sizeof(llist));
    list->size = 0;
    list->head = NULL;
    return list;
}

uint64_t llist_size(llist *list)
{
    return list->size;
}

llist_node *llist_node_create(void *data)
{
    llist_node *new_node = (llist_node *)calloc(1, sizeof(llist_node));
    new_node->data = data;
}

void llist_insertHead(llist *list, llist_node *new_node)
{
    if (new_node != NULL) {
        new_node->next = list->head;
        list->head = new_node;
        list->size++;
    }
}

llist_node *llist_removeHead(llist *list)
{
    if (list->head == NULL)
        return NULL;
    llist_node *removed_node = list->head;
    list->head = list->head->next;
    list->size--;
    return removed_node;
}

void llist_insertAfter(llist *list, llist_node *node, llist_node *new_node)
{
    if (node != NULL && new_node != NULL) {
        new_node->next = node->next;
        node->next = new_node;
        list->size++;
    }
}

llist_node *llist_removeAfter(llist *list, llist_node *node)
{
    if (node == NULL)
        return NULL;
    llist_node *removed_node = node->next;
    node->next = node->next->next;
    list->size--;
    return removed_node;
}
