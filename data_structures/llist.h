#ifndef _LLIST_H_
#define _LLIST_H_
#include <stdint.h>

typedef struct llist_node {
    void *data;
    struct llist_node *next;
} llist_node;

typedef struct llist {
    uint64_t size;
    llist_node *head;
} llist;

uint64_t llist_size(llist *list);
llist_node *llist_node_create(void *data);
void llist_insertHead(llist *list, llist_node *new_node);
llist_node *llist_removeHead(llist *list);
void llist_insertAfter(llist *list, llist_node *node, llist_node *new_node);
llist_node *llist_removeAfter(llist *list, llist_node *node);

#endif
